package task1_explicit_lock;

/**
 * Task 1. Modify exercise 6 from previous presentation
 * to use explicit Lock objects.
 */
public class App {

  public static void main(String[] args) {
    LockObjects lockObjects = new LockObjects();
    lockObjects.startLockObjects();
  }
}
