package task1_explicit_lock;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockObjects {

  private Lock commonLock = new ReentrantLock();

  public void startLockObjects() {
    System.out.println("1 - Methods with common lock");
    System.out.println("2 - Methods with their own different locks");
    Scanner input = new Scanner(System.in);
    while (true) {
      try {
        System.out.println("Your choice: ");
        String mode = input.next();
        switch (Integer.valueOf(mode)) {
          case 1:
            executeCommonLockMode();
            return;
          case 2:
            executeDifferentLockMode();
            return;
        }
      } catch (IllegalArgumentException e) {
      }
    }
  }

  private void executeCommonLockMode() {
    ExecutorService executor;
    for (int i = 0; i < 2; i++) {
      executor = Executors.newFixedThreadPool(3);
      executor.submit(this::object1Attempt);
      executor.submit(this::object2Attempt);
      executor.submit(this::object3Attempt);
      try {
        Tools.await(executor);
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private void executeDifferentLockMode() {
    ExecutorService executorService;
    executorService = Executors.newCachedThreadPool();
    executorService.submit(this::ownLockMethod1);
    executorService.submit(this::ownLockMethod2);
    executorService.submit(this::ownLockMethod3);
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      Tools.await(executorService);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void object1Attempt() {
    String object1 = "Object 1";
    commonLock.lock();
    getLock(object1);
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    lostLock(object1);
    commonLock.unlock();
  }

  private void object2Attempt() {
    String object2 = "Object 2";
    commonLock.lock();
    getLock(object2);
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    lostLock(object2);
    commonLock.unlock();
  }

  private void object3Attempt() {
    String object3 = "Object 3";
    commonLock.lock();
    getLock(object3);
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    lostLock(object3);
    commonLock.unlock();
  }

  private void lostLock(String objectName) {
    System.out.println(objectName + " doesn't have lock");
    System.out.println("************************");
  }

  private void getLock(String objectName) {
    System.out.println(objectName + " has common lock");
  }

  private void ownLockMethod1() {
    Lock lock = new ReentrantLock();
    lock.lock();
    showExecution(1);
    lock.unlock();
  }

  private void ownLockMethod2() {
    Lock lock = new ReentrantLock();
    lock.lock();
    showExecution(2);
    lock.unlock();
  }

  private void ownLockMethod3() {
    Lock lock = new ReentrantLock();
    lock.lock();
    showExecution(3);
    lock.unlock();
  }

  private void showExecution(int m) {
    System.out.println(
        "TIME: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) +
            " | THREAD <" + Thread.currentThread().getName() + "> | METHOD " + m);
  }
}
