package task2_blocking_queue;

/**
 * Task 2. Modify exercise 7 from previous presentation to use a
 * MyBlockingQueue instead of a pipe.
 */
public class App {

  public static void main(String[] args) {
    MyBlockingQueue myblockingQueue = new MyBlockingQueue();
    myblockingQueue.startBlockingQueue();
  }
}
