package task2_blocking_queue;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyBlockingQueue {

  private final int SIZE = 10;
  private java.util.concurrent.BlockingQueue<String> pipe = new ArrayBlockingQueue<>(SIZE);
  private String[] someStuff = {"One", "Two", "Three", "Four", "Five", "Six", "Seven",
      "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen"};

  public void startBlockingQueue() {
    ExecutorService executorService = Executors.newFixedThreadPool(2);
    executorService.submit(this::putToPipe);
    executorService.submit(this::takeFromPipe);
    try {
      Tools.await(executorService);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("**** STUFF IN PIPE ****");
    pipe.forEach(s -> System.out.print(s + " "));
  }

  private void putToPipe() {
    try {
      for (String stuff : someStuff) {
        if (pipe.size() == SIZE) {
          System.out.println("PIPE IS FULL! PUT: " + stuff);
          Thread.sleep(3000);
        }
        pipe.put(stuff);
        showStuff("<- ", stuff, pipe.size());
        Thread.sleep(800);
      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void takeFromPipe() {
    try {
      Thread.sleep(7000);
      for (int i = 0; i < SIZE + 2; i++) {
        if (pipe.isEmpty()) {
          System.out.println("PIPE IS EMPTY!");
          Thread.sleep(5000);
        }
        String stuff = pipe.take();
        showStuff("-> ", stuff, pipe.size());
        Thread.sleep(200);
      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void showStuff(String symb, String stuff, int pipeSize) {
    System.out
        .println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd MMMyyр HH:mm:ss;"))
            + " THREAD <" + Thread.currentThread().getName() + "> | " + symb + stuff + " SIZE: "
            + pipeSize);
  }

}