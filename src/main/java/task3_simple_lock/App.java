package task3_simple_lock;

/**
 * Task 3. Create your own ReadWriteLock (or at least simple Lock).
 */
public class App {

  public static void main(String[] args) {
    SimpleLock simpleLock = new SimpleLock();

    for (int i = 0; i < 5; i++) {
      new Thread(() -> {

        simpleLock.lock();
        start();
        simpleLock.unlock();

      }).start();
    }
  }

  private static void start() {
    System.out.println(Thread.currentThread());
    try {
      Thread.sleep(500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(Thread.currentThread());
  }
}
